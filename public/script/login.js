var btnLogin = document.getElementById("idLogin");

btnLogin.onclick = async function(){
    await salvarUser();
};

// Salvar os dados de usuário para verificar no backend
async function salvarUser(){
    localStorage.removeItem('token');
    let userId = document.getElementById("idUsuario").value;
    let pass = document.getElementById("idSenha").value;

    const update = {
        userId: userId,
        pass: pass
    }
    const options = {
        method: "POST",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(update)
    };

    let response = await buscarToken(options);
    let tokenResp = await response.json();
    let autTrue = tokenResp.auth;
    
    // Verificação do login e retorno do token
    if (autTrue == true) {
        let token = tokenResp.token;
        localStorage.setItem('token', token);
        
        window.location.href = "http://localhost:3001/"
    };
};

async function buscarToken(options) {
    var response = await fetch("http://localhost:3001/login", options);

    return response;
};