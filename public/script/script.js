var modalBody = document.getElementById("idBodyModal");
var form = document.getElementById("idForm");
var fecharModal = document.getElementById("idFecharModal");

function pegarTimestamp() {
    let data = new Date();
    let dia = String(data.getDate()).padStart(2, '0');
    let mes = String(data.getMonth() + 1).padStart(2, '0');
    let ano = data.getFullYear();
    let hora = data.getHours();
    let minutos = data.getMinutes();
    let segundos = data.getSeconds();
    let milisegundos = data.getMilliseconds();
    var timestamp = ano + mes + dia + hora + minutos + segundos + milisegundos;

    return timestamp
}

btnEnviar = document.getElementById("idEnviar");
btnEnviar.onclick = async function () {
    await salvarDados();
}

async function salvarDados() {
    // Buscando token de login
    let token = window.localStorage.getItem('token');

    // Pegando os dados da tela 
    let soliAtendida = document.querySelector("#idSelect1").value;
    let notaAtendimento = document.querySelector("#idSelect2").value;
    let compAtendente = document.querySelector("#idSelect3").value;
    let notaProduto = document.querySelector("#idRange").value;
    let idade = document.querySelector("#idIdade").value;
    let genero = document.querySelector("input[name=nmRadio]:checked").value;

    const update = {
        soliAtendida: soliAtendida,
        notaAtendimento: notaAtendimento,
        compAtendente: compAtendente,
        notaProduto: notaProduto,
        idade: idade,
        genero: genero,
        timestamp: pegarTimestamp()
    }

    const options = {
        method: "POST",
        headers: {
            "content-type": "application/json",
            "x-access-token": token
        },
        body: JSON.stringify(update)
    }

    //fazer uma requisição ao Backend para salvar os dados
    let resposta = await salvar(options);
    let mensagem = await resposta.json();

    if (mensagem.Status == "200") {
        resetar();
        form.classList.add("visually-hidden");
        modalBody.innerHTML = "Status: " + mensagem.Status + "<br>" + "Mensagem: " + mensagem.Mensagem;
        setTimeout(function () {
            $('.modal').modal('hide');
            form.classList.remove("visually-hidden");
        }, 2000)
        fecharModal.onclick = function () {
            form.classList.remove("visually-hidden");
        }
    } else if (mensagem.Status == "500") {
        resetar();
        form.classList.add("visually-hidden");
        modalBody.classList.add("text-danger");
        modalBody.innerHTML = "Status: " + mensagem.Status + "<br>" + "Mensagem: " + mensagem.Mensagem;
        fecharModal.onclick = function () {
            form.classList.remove("visually-hidden");
        }
    } else {
        resetar();
        modalBody.innerHTML = "Ocorreu um erro, tente novamente.";
    }
}

// Salvar os dados 
async function salvar(options) {
    var response = await fetch("http://localhost:3001/pesquisa", options);

    return response;
}

// Resetar campos
function resetar() {
    form.reset();
}