const express = require("express");
const fs = require("fs");
const cors = require("cors");
const path = require("path");
const jwt = require("jsonwebtoken");
const SECRET = "segredo";

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(path.join(__dirname, '/public')));
app.use(cors());


const porta = 3001;
app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
});


// Página inicial
app.get("/", function (req, resp) {
    resp.sendFile(path.join(__dirname + "/view/index.html"))
});


// Salvando os dados 
app.post("/pesquisa", verifyJWT, function (req, resp) {

    let dados = req.body.soliAtendida + "," + req.body.notaAtendimento + "," + req.body.compAtendente + "," + req.body.notaProduto + "," + req.body.idade + "," + req.body.genero + "," + req.body.timestamp + "\n"

    fs.appendFile("pesquisa.csv", dados, function (err) {
        if (err) {
            resp.json(
                {
                    "Status": "500",
                    "Mensagem": err
                });
        } else {
            resp.json(
                {
                    "Status": "200",
                    "Mensagem": "Pesquisa Registrada com Sucesso"
                });
        }
    });
});

// Lendo os dados
app.get("/estatisticas", function (req, resp) {

    fs.readFile('pesquisa.csv', 'utf8', function (err, data) {
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            let dados = data.split("\n");
            let dadosPesquisa = [];

            dados.forEach(element => {
                dadosPesquisa.push(element.split(","));
            });
            var estatisticas = calcEstatistica(dadosPesquisa);
            resp.json(estatisticas);
        };
    });
});

// Calculando as estatísticas
function calcEstatistica(dadosPesquisa){
    
    var qtdRespostas = dadosPesquisa.length - 1;
    let idadeTemp;
    let soliAtendida = [0, 0, 0];
    let notaAtendimento = [0, 0, 0, 0, 0];
    let compAtendente = [0, 0, 0, 0, 0];
    let notaProduto = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let idade = [0, 0, 0, 0, 0];
    let genero = [0, 0, 0];

    for (let i = 0; i < dadosPesquisa.length - 1; i++) {
        //Questão a
        switch (dadosPesquisa[i][0]) {
            case "Totalmente Atendida":
                soliAtendida[0]++
                break;
            case "Parcialmente Atendida":
                soliAtendida[1]++
                break;
            case "Não Atendida":
                soliAtendida[2]++
                break;
        }
        //Questão b
        switch (dadosPesquisa[i][1]) {
            case "Excelente":
                notaAtendimento[0]++
                break;
            case "Bom":
                notaAtendimento[1]++
                break;
            case "Aceitável":
                notaAtendimento[2]++
                break;
            case "Ruim":
                notaAtendimento[3]++
                break;
            case "Péssimo":
                notaAtendimento[4]++
                break;
        }
        //Questão c
        switch (dadosPesquisa[i][2]) {
            case "Muito Atencioso":
                compAtendente[0]++
                break;
            case "Educado":
                compAtendente[1]++
                break;
            case "Neutro":
                compAtendente[2]++
                break;
            case "Mau humorado":
                compAtendente[3]++
                break;
            case "Indelicado":
                compAtendente[4]++
                break;
        }
        //Questão d
        switch (dadosPesquisa[i][3]) {
            case "0":
                notaProduto[0]++
                break;
            case "1":
                notaProduto[1]++
                break;
            case "2":
                notaProduto[2]++
                break;
            case "3":
                notaProduto[3]++
                break;
            case "4":
                notaProduto[4]++
                break;
            case "5":
                notaProduto[5]++
                break;
            case "6":
                notaProduto[6]++
                break;
            case "7":
                notaProduto[7]++
                break;
            case "8":
                notaProduto[8]++
                break;
            case "9":
                notaProduto[9]++
                break;
            case "10":
                notaProduto[10]++
                break;
        }
        //Questão e
        if (dadosPesquisa[i][4] < 15) {
            idadeTemp = 0;
        } else if (dadosPesquisa[i][4] >= 15 && dadosPesquisa[i][4] <= 21) {
            idadeTemp = 1;
        } else if (dadosPesquisa[i][4] >= 22 && dadosPesquisa[i][4] <= 35) {
            idadeTemp = 2;
        } else if (dadosPesquisa[i][4] >= 36 && dadosPesquisa[i][4] <= 50) {
            idadeTemp = 3;
        } else if (dadosPesquisa[i][4] > 50) {
            idadeTemp = 4;
        }
        switch (idadeTemp) {
            case 0:
                idade[0]++
                break;
            case 1:
                idade[1]++
                break;
            case 2:
                idade[2]++
                break;
            case 3:
                idade[3]++
                break;
            case 4:
                idade[4]++
                break;
        }
        //Questão f
        switch (dadosPesquisa[i][5]) {
            case "Feminino":
                genero[0]++
                break;
            case "Masculino":
                genero[1]++
                break;
            case "LGBT":
                genero[2]++
                break;
        }
    }
    // Montando a resposta
    var pesquisa = {
        "a": {
            "totalmenteAtendida": calcPorcent(soliAtendida[0], qtdRespostas),
            "parcialmenteAtendida": calcPorcent(soliAtendida[1], qtdRespostas),
            "nãoFoiAtendida": calcPorcent(soliAtendida[2], qtdRespostas)
        },
        "b": {
            "excelente": calcPorcent(notaAtendimento[0], qtdRespostas),
            "bom": calcPorcent(notaAtendimento[1], qtdRespostas),
            "aceitável": calcPorcent(notaAtendimento[2], qtdRespostas),
            "ruim": calcPorcent(notaAtendimento[3], qtdRespostas),
            "péssimo": calcPorcent(notaAtendimento[4], qtdRespostas)
        },
        "c": {
            "muitoAtencioso": calcPorcent(compAtendente[4], qtdRespostas),
            "educado": calcPorcent(compAtendente[3], qtdRespostas),
            "neutro": calcPorcent(compAtendente[2], qtdRespostas),
            "mauHumorado": calcPorcent(compAtendente[1], qtdRespostas),
            "indelicado": calcPorcent(compAtendente[0], qtdRespostas)
        },
        "d": {
            "zero": calcPorcent(notaProduto[0], qtdRespostas),
            "um": calcPorcent(notaProduto[1], qtdRespostas),
            "dois": calcPorcent(notaProduto[2], qtdRespostas),
            "tres": calcPorcent(notaProduto[3], qtdRespostas),
            "quatro": calcPorcent(notaProduto[4], qtdRespostas),
            "cinco": calcPorcent(notaProduto[5], qtdRespostas),
            "seis": calcPorcent(notaProduto[6], qtdRespostas),
            "sete": calcPorcent(notaProduto[7], qtdRespostas),
            "oito": calcPorcent(notaProduto[8], qtdRespostas),
            "nove": calcPorcent(notaProduto[9], qtdRespostas),
            "dez": calcPorcent(notaProduto[10], qtdRespostas)
        },
        "e": {
            "menorQue15": calcPorcent(idade[0], qtdRespostas),
            "entre15e21": calcPorcent(idade[1], qtdRespostas),
            "entre22e35": calcPorcent(idade[2], qtdRespostas),
            "entre36e50": calcPorcent(idade[3], qtdRespostas),
            "maiorQue50": calcPorcent(idade[4], qtdRespostas)
        },
        "f": {
            "feminino": calcPorcent(genero[0], qtdRespostas),
            "masculino": calcPorcent(genero[1], qtdRespostas),
            "LGBT": calcPorcent(genero[2], qtdRespostas)
        }
    }
    return pesquisa
}

// Cálculo da porcentagem
function calcPorcent(valor, qtdRespostas) {
    return ((valor / qtdRespostas) * 100)
}

// Mostrando o relatório 
app.get("/relatorio", function (req, resp) {
    var dadosPesquisa = [];

    fs.readFile('pesquisa.csv', 'utf8', function (err, data) {
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            let dados = data.split("\n");

            dados.forEach(element => {
                dadosPesquisa.push(element.split(","));
            });

            var estatisticas = calcEstatistica(dadosPesquisa);

            resp.send(
                `
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
                        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
                    <title>Relatório</title>
                </head>
                <body class="container">
                    <p class="fs-4 my-2">Relatório</h2>
                    <div class="row mb-3">
                        <p>a) Solicitação Atendida?</p>
                        <p>Totalmente Atendida: ${estatisticas.a.totalmenteAtendida.toFixed()}%</p>
                        <p>Parcialmente atendida: ${estatisticas.a.parcialmenteAtendida.toFixed()}%</p>
                        <p>Não foi atendida: ${estatisticas.a.nãoFoiAtendida.toFixed()}%</p>
                    </div>
                    <div class="row mb-3">
                        <p>b) Nota do Atendimento?</p>
                        <p>Excelente: ${estatisticas.b.excelente.toFixed()}%</p>
                        <p>Bom: ${estatisticas.b.bom.toFixed()}%</p>
                        <p>Aceitável: ${estatisticas.b.aceitável.toFixed()}%</p>
                        <p>Ruim: ${estatisticas.b.ruim.toFixed()}%</p>
                        <p>Péssimo: ${estatisticas.b.péssimo.toFixed()}%</p>
                    </div>
                    <div class="row mb-3">
                        <p>c) Comportamento do Atendente?</p>
                        <p>Muito Atencioso: ${estatisticas.c.muitoAtencioso.toFixed()}%</p>
                        <p>Educado: ${estatisticas.c.educado.toFixed()}%</p>
                        <p>Neutro: ${estatisticas.c.neutro.toFixed()}%</p>
                        <p>Mau Humorado: ${estatisticas.c.mauHumorado.toFixed()}%</p>
                        <p>Indelicado: ${estatisticas.c.indelicado.toFixed()}%</p>
                    </div>
                    <div class="row mb-3">
                        <p>d) Nota do Produto?</p>
                        <p>0: ${estatisticas.d.zero.toFixed()}%</p>
                        <p>1: ${estatisticas.d.um.toFixed()}%</p>
                        <p>2: ${estatisticas.d.dois.toFixed()}%</p>
                        <p>3: ${estatisticas.d.tres.toFixed()}%</p>
                        <p>4: ${estatisticas.d.quatro.toFixed()}%</p>
                        <p>5: ${estatisticas.d.cinco.toFixed()}%</p>
                        <p>6: ${estatisticas.d.seis.toFixed()}%</p>
                        <p>7: ${estatisticas.d.sete.toFixed()}%</p>
                        <p>8: ${estatisticas.d.oito.toFixed()}%</p>
                        <p>9: ${estatisticas.d.nove.toFixed()}%</p>
                        <p>10: ${estatisticas.d.dez.toFixed()}%</p>
                    </div>
                    <div class="row mb-3">
                        <p>e) Idade?</p>
                        <p>Menor que 15: ${estatisticas.e.menorQue15.toFixed()}%</p>
                        <p>Entre 15 e 21: ${estatisticas.e.entre15e21.toFixed()}%</p>
                        <p>Entre 22 e 35: ${estatisticas.e.entre22e35.toFixed()}%</p>
                        <p>Entre 36 e 50: ${estatisticas.e.entre36e50.toFixed()}%</p>
                    </div>
                    <div class="row mb-3">
                        <p>f) Sexo?</p>
                        <p>Feminino: ${estatisticas.f.feminino.toFixed()}%</p>
                        <p>Masculino: ${estatisticas.f.masculino.toFixed()}%</p>
                        <p>LGBT: ${estatisticas.f.LGBT.toFixed()}%</p>
                    </div>
                </body>
                </html>
                `
            );
        };
    });
});

// Tela de Login 
app.get("/login", function(req, resp) {
    resp.sendFile(path.join(__dirname + "/view/login.html"));
});

// Leitura dos logins cadastrados e comparação com login informado pelo usuário
app.post("/login", function (req, resp) {
    var userId = req.body.userId;
    var pass = req.body.pass;

    fs.readFile('usuarios.csv', 'utf8', function(err, data){
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            let dados = data.split("\r\n");
            let dadosUser = [];

            dados.forEach(element => {
                dadosUser.push(element.split(","));
            });
            var autentica = autenticacao(dadosUser, userId, pass);

            if (autentica == true) {
                //gerar token 
                const token = jwt.sign({userId: req.body.user}, SECRET, {expiresIn: 600})
                return resp.json({auth: true, token});
            } else {
                resp.status(401).end();
            };
        };
    });
});

// Autenticação dos dados
function autenticacao(dadosUser, userId, pass){

    for (let i = 0; i < dadosUser.length - 1; i++) {
        if (userId == dadosUser[i][0] && pass == dadosUser[i][1]) {
            autentica = true;
            break;
        } else {
            autentica = false;
        }
    };
    return autentica
};

// Criação do Middleware de Autenticação
function verifyJWT(req, resp, next) {
    const token = req.header("x-access-token");

    jwt.verify(token, SECRET, function(err, decoded){
        if (err) {
            return resp.status(401).end();
        };
        next();
    });
};